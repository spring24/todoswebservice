package com.in28minutes.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodosWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodosWebServiceApplication.class, args);
	}

}
