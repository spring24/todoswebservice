package com.in28minutes.webservices.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.in28minutes.webservices.security.models.AuthenticationRequest;
import com.in28minutes.webservices.security.models.AuthenticationResponse;
import com.in28minutes.webservices.security.util.JwtUtil;

@RestController
@CrossOrigin(origins="http://localhost:3000")
public class JwtResource {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private MyUserDetailsService userDetailService;
	
	@Autowired
	private JwtUtil jwtTokenUtil;

	@RequestMapping("/hello")
	public String hello() {
		return "Hello World";
	}

	@RequestMapping(value="/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
		//¿POR QUÉ ES NECESARIO USAR AUTHENTICATION_MANAGER.authenticate?
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),authenticationRequest.getPassword()));
		}catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password");
		}
		
		final UserDetails userDetails = userDetailService.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
	
	@RequestMapping(value="/refresh", method = RequestMethod.GET)
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) throws Exception {
		String authenticationToken = request.getHeader("Authorization");
		final String token = authenticationToken.substring(7);
		String username = jwtTokenUtil.extractUsername(token);
		UserDetails userDetails = userDetailService.loadUserByUsername(username);
		
		if(jwtTokenUtil.validateToken(token, userDetails)) {
			String refreshToken = jwtTokenUtil.refreshToken(token);
			 return ResponseEntity.ok(new AuthenticationResponse(refreshToken));
	    } else {
	      return ResponseEntity.badRequest().body(null);
	    }
	}
}
