package com.in28minutes.webservices.security.models;

public class AuthenticationResponse {
	private final String token;

	public AuthenticationResponse(String jwt) {
		this.token = jwt;
	}

	public String getToken() {
		return token;
	}
	
}
